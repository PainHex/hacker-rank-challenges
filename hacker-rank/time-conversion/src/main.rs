use std::io;

fn main() {

    let mut time_string = String::new();

    io::stdin().read_line(&mut time_string).ok().expect("Read error");

    let mut chars = time_string.chars();

    let hours: String = chars.by_ref().take(2).collect();
    let minutes: String = chars.by_ref().skip(1).take(2).collect();
    let seconds: String = chars.by_ref().skip(1).take(2).collect();
    let period: String = chars.by_ref().take(2).collect();

    println!("{:02}:{}:{}", get_hour(&hours, period), minutes, seconds);
}

fn get_hour(string_value: &String, period: String) -> i32 {
    let mut hour = string_value.parse().unwrap();

    if period == "PM".to_string() && hour < 12 {
        hour += 12;
    }

    if hour >= 24 {
        hour -= 24;
    }

    if hour >= 12 && period == "AM".to_string() {
        hour -= 12;
    }

    hour
}