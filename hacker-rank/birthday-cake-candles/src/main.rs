use std::io;

fn main() {

    let mut candle_count_input = String::new();
    let mut candle_height_input = String::new();

    io::stdin().read_line(&mut candle_count_input).ok().expect("Read error");
    io::stdin().read_line(&mut candle_height_input).ok().expect("Read error");

    let candle_heights = get_int_values(candle_height_input);
    let max_height = *candle_heights.iter().max().unwrap();

    let blown_candles: usize = candle_heights.iter()
        .filter(|&c| *c == max_height)
        .count();

    println!("{}", blown_candles);
}


fn get_int_values(input: String) -> Vec<i32> {
    input.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect()
}

fn get_int(string_value: &String) -> i32 {
    string_value.trim().parse().unwrap()
}