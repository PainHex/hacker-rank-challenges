use std::io;

fn main() {

    let mut array_size = String::new();
    let mut array = String::new();

    io::stdin().read_line(&mut array_size).ok().expect("Read error");
    io::stdin().read_line(&mut array).ok().expect("Read error");

    let array_values = get_int_values(array);

    let positive_ratio = array_values.iter()
        .filter(|&i| *i > 0)
        .count() as f32 / get_int(&array_size) as f32;

    let zero_ratio = array_values.iter()
        .filter(|&i| *i == 0)
        .count() as f32 / get_int(&array_size) as f32;

    let negative_ratio = array_values.iter()
        .filter(|&i| *i < 0)
        .count() as f32 / get_int(&array_size) as f32;

    println!("{}", positive_ratio);
    println!("{}", negative_ratio);
    println!("{}", zero_ratio);
}

fn get_int_values(input: String) -> Vec<i32> {
    input.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect()
}

fn get_int(string_value: &String) -> i32 {
    string_value.trim().parse().unwrap()
}