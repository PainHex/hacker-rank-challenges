use std::io;

fn main() {

    let mut input_length = String::new();
    let mut inputs = String::new();

    io::stdin().read_line(&mut input_length).ok().expect("read error");
    io::stdin().read_line(&mut inputs).ok().expect("read error");

    let sum: i64 = get_int_values(inputs).iter()
        .fold(0, |sum, i| sum + *i as i64);

    println!("{}", sum);
}

fn get_int_values(input: String) -> Vec<i32> {
    input.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect()
}