use std::io;

fn main() {

    let mut staircase_size = String::new();

    io::stdin().read_line(&mut staircase_size).ok().expect("Read error");

    let staircase_size_int = get_int(&staircase_size);

    for i in 0..staircase_size_int {
        println!("{}{}", " ".repeat((staircase_size_int - 1 - i) as usize), "#".repeat((i + 1) as usize));
    }

}

fn get_int(string_value: &String) -> i32 {
    string_value.trim().parse().unwrap()
}