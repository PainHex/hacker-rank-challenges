use std::io;
fn main() {
    let mut _num_str_1 = String::new();
    let mut _num_str_2 = String::new();

    io::stdin().read_line(&mut _num_str_1).ok().expect("read error");
    io::stdin().read_line(&mut _num_str_2).ok().expect("read error");

    let mut num_1 : i32 = _num_str_1.trim().parse().ok().expect("parse error");
    let mut num_2 : i32 = _num_str_2.trim().parse().ok().expect("parse error");

    println!("{}", num_1 + num_2);
}