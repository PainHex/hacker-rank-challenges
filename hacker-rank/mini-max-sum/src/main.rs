use std::io;

fn main() {

    let mut value_input = String::new();

    io::stdin().read_line(&mut value_input).ok().expect("Read error");

    let mut values = get_int_values(value_input);
    values.sort();

    let min: i64 = values.iter()
        .take(4)
        .sum();

    values.reverse();
    let max: i64 = values.iter()
        .take(4)
        .sum();

    println!("{} {}", min, max);
}

fn get_int_values(input: String) -> Vec<i64> {
    input.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect()
}
