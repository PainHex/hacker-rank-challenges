use std::io;

fn main() {
    let mut binary_string_size = String::new();
    let mut binary_string = String::new();

    io::stdin().read_line(&mut binary_string_size).ok().expect("Read error");
    io::stdin().read_line(&mut binary_string).ok().expect("Read error");

    println!("{}", binary_string.matches("010").count());
}