use std::io;
fn main() {
    let mut input_one = String::new();
    let mut input_two = String::new();

    io::stdin().read_line(&mut input_one).ok().expect("read error");
    io::stdin().read_line(&mut input_two).ok().expect("read error");

    let num_1 : i32 = input_one.trim().parse().ok().expect("parse error");
    let sum : Vec<i32> = input_two.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect();

    println!("{}", sum.iter().sum::<i32>());
}