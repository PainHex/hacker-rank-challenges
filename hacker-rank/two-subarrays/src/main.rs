use std::io;

fn main() {

    let mut array_size_input = String::new();
    let mut array_elements_input = String::new();

    io::stdin().read_line(&mut array_size_input).ok().expect("Read error");
    io::stdin().read_line(&mut array_elements_input).ok().expect("Read error");

    let mut size = get_int(array_size_input);
    let mut values = get_int_values(array_elements_input);



}

fn sum(values: Vec<i32>) -> i32 {

}

fn get_int_values(input: String) -> Vec<i32> {
    input.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect()
}

fn get_int(string_value: &String) -> i32 {
    string_value.trim().parse().unwrap()
}