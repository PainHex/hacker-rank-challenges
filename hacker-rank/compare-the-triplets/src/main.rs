use std::io;

fn main() {

    let mut alice = String::new();
    let mut bob = String::new();

    io::stdin().read_line(&mut alice).ok().expect("read error");
    io::stdin().read_line(&mut bob).ok().expect("read error");

    let alice_points = get_points(alice);
    let bob_points = get_points(bob);

    let alice_total = get_comparison_points(&alice_points, &bob_points);
    let bob_total = get_comparison_points(&bob_points, &alice_points);

    println!("{} {}", alice_total, bob_total);
}

fn get_points(input: String) -> Vec<i32> {
    input.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect()
}

fn get_comparison_points(a: &Vec<i32>, b: &Vec<i32>) -> usize {
    a.iter()
        .zip(b.iter())
        .filter(|number| number.0 > number.1)
        .count()
}