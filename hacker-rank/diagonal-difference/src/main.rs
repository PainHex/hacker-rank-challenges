use std::io;

fn main() {

    let mut size_input = String::new();

    io::stdin().read_line(&mut size_input).ok().expect("Read error");

    let size : i32 = size_input.trim().parse().unwrap();
    let mut diagonal_difference = 0;

    for i in 0..size {
        let mut input = String::new();
        io::stdin().read_line(&mut input).ok().expect("Read error");

        let numbers = get_int_values(input);

        diagonal_difference += numbers[i as usize] - numbers[(size - 1 - i) as usize];
    }

    println!("{}", diagonal_difference.abs());
}

fn get_int_values(input: String) -> Vec<i32> {
    input.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect()
}