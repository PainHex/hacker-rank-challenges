fn get_int_values(input: String) -> Vec<i32> {
    input.split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect()
}

fn get_int(string_value: &String) -> i32 {
    string_value.trim().parse().unwrap()
}