use std::io;
use std::io::Read;

fn main(){

    let mut frequency_inputs = String::new();

    io::stdin().read_to_string(&mut frequency_inputs).ok().expect("Read error");

    println!("{}", frequency_inputs);

    let mut total = 0;

    for line in frequency_inputs.split('\n') {
        if !line.is_empty() {
            let (operator, value) = line.split_at(1);

           match operator {
               "+" => total = total + value.parse::<i32>().unwrap(),
               "-" => total = total - value.parse::<i32>().unwrap(),
               _ => total = total
           }
        }
    }

    println!("Total: {:?}", total);
}

fn get_int(string_value: &String) -> i32 {
    string_value.trim().parse().unwrap()
}